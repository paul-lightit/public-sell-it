var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');

gulp.task('html', function(){
    return gulp.src('./app/src/**/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('./app/dist/'))
});

gulp.task('css', function(){
    return gulp.src('./app/src/**/style.sass')
        .pipe(sass({includePaths: require('node-normalize-scss').includePaths}))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./app/dist/css'))
});

gulp.task('all:watch', function () {
    gulp.watch('./app/src/**/*.pug', ['html']);
    gulp.watch('./app/src/**/*.sass', ['css']);
});

gulp.task('default', [ 'html', 'css', 'all:watch' ]);